<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\AnswerModel;

class AnswerController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionAdd()
    {
        $qid = Yii::$app->request->get('qid');
        $arr = array("A", "B", "C", "D");
        foreach ($arr as &$item) {
            $code = $item;
            $content = '哈哈哈哈';
            $value = '0';
    
            $model = new AnswerModel;
            $model->qid = $qid;
            $model->code = $code;
            $model->content = $content;
            $model->value = $value;
            
            $model->save();
        }

        $data = array(
            'code' => 0,
            'message' => '新增成功',
            'result' => ''
        );
        $json = json_encode($data);
        return $json;
    }
    public function actionGetanswers()
    {
        $model = AnswerModel::find()
            ->asArray()
            ->all();

        $json = json_encode($model);
        return $json;
    }
    public function actionGetanswersbyqid()
    {
        $qid = Yii::$app->request->post('qid');
        $model = AnswerModel::find()
            ->where(['qid' => $qid])
            ->orderBy('id ASC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetjsonbyid()
    {
        $qid = Yii::$app->request->get('id');
        $model = AnswerModel::find()
            ->select(['code', 'content', 'value'])
            ->where(['qid' => $qid])
            ->orderBy('id ASC')
            ->asArray()
            ->all();

        $json =  json_encode($model, JSON_UNESCAPED_UNICODE);
        echo($json);
    }    
}
