<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\BorrowModel;

class BorrowController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionAddborrow()
    {
        $openid = Yii::$app->request->post('openid');
        $name = Yii::$app->request->post('name');
        $userinfo = Yii::$app->request->post('userinfo');
        $phoneid = Yii::$app->request->post('phoneid');
        $phoneinfo = Yii::$app->request->post('phoneinfo');
        $action = Yii::$app->request->post('action');
    

        // 新建一条记录
        $model = new BorrowModel;
        $model->openid = $openid;
        $model->name = $name;
        $model->userinfo = $userinfo;
        $model->phoneid = $phoneid;
        $model->phoneinfo = $phoneinfo;
        $model->action = $action;
        
        $model->save();

        $data = array(
            'code' => 0,
            'message' => '新增成功',
            'result' => $model
        );
        $json = json_encode($data);
        return $json;
    }


    public function actionGetborrowbyphoneid()
    {
        $phoneid = Yii::$app->request->post('phoneid');
        $model = BorrowModel::find()
            ->where(['phoneid' => $phoneid])
            ->orderBy('id DESC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
}
