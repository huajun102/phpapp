<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\ConfModel;

class ConfController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionAdd()
    {
        $openid = Yii::$app->request->post('openid');
        $value = Yii::$app->request->post('value');

        $model = new ConfModel;
        $model->openid = $openid;
        $model->value = $value;
        
        $model->save();
        
        $id =$model->id;
        $model = ConfModel::find()
            ->where(['id' => $id])
            ->orderBy('id DESC')
            ->asArray()
            ->one();

        $data = array(
            'code' => 0,
            'message' => '新增成功',
            'result' => $model
        );
        $json = json_encode($data);
        return $json;
    }    
    public function actionGetall()
    {
        $model = ConfModel::find()
            ->orderBy('id ASC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetbyopenid()
    {
        $openid = Yii::$app->request->post('openid');
        $model = ConfModel::find()
            ->where(['openid' => $openid])
            ->orderBy('id DESC')
            ->asArray()
            ->one();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }    
}
