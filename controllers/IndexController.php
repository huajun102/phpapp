<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\UploadForm;
use yii\web\UploadedFile;


class IndexController extends Controller
{

    public function actionIndex()
    {
        $date = date('Y-m-d H:i:s');
        return $date;
    }
    public function actionTest()
    {
        return $this->render('test');
    }
    public function actionHello()
    {
        var_dump(Yii::$app->request->post());
    }
    public function actionUpload()
    {
        $model = new UploadForm();
        var_dump(123);
        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->upload()) {
                // 文件上传成功
                return;
            }
        }

//        return $this->render('upload', ['model' => $model]);
    }
}
