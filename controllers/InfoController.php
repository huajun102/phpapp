<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\InfoModel;

class InfoController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionAdd()
    {
        $name = Yii::$app->request->post('name');
        $phone = Yii::$app->request->post('phone');
        $content = Yii::$app->request->post('content');
        $address = Yii::$app->request->post('address');
        $image = Yii::$app->request->post('image');


        // 新建一条记录
        $model = new InfoModel;
        $model->name = $name;
        $model->phone = $phone;
        $model->content = $content;
        $model->address = $address;
        $model->image = $image;
        
        $model->save();
        
        $id =$model->id;
        $model = InfoModel::find()
            ->where(['id' => $id])
            ->orderBy('id DESC')
            ->asArray()
            ->one();

        $data = array(
            'code' => 0,
            'message' => '新增成功',
            'result' => $model
        );
        $json = json_encode($data);
        return $json;
    }
    public function actionGetbyid()
    {
        $id = Yii::$app->request->post('id');
        $model = InfoModel::find()
            ->where(['id' => $id])
            ->asArray()
            ->one();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetall()
    {
        $model = InfoModel::find()
            ->orderBy('id DESC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
}
