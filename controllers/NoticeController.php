<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\NoticeModel;

class NoticeController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionAdd()
    {
        $image = Yii::$app->request->post('image');
        $title = Yii::$app->request->post('title');
        $content = Yii::$app->request->post('content');
        $tag1 = Yii::$app->request->post('tag1');
        $tag2 = Yii::$app->request->post('tag2');


        // 新建一条记录
        $model = new NoticeModel;
        $model->image = $image;
        $model->title = $title;
        $model->content = $content;
        $model->tag1 = $tag1;
        $model->tag2 = $tag2;
        
        $model->save();
        
        $id =$model->id;
        $model = NoticeModel::find()
            ->where(['id' => $id])
            ->orderBy('id DESC')
            ->asArray()
            ->one();

        $data = array(
            'code' => 0,
            'message' => '新增成功',
            'result' => $model
        );
        $json = json_encode($data);
        return $json;
    }
    public function actionGetbyid()
    {
        $id = Yii::$app->request->post('id');
        $model = NoticeModel::find()
            ->where(['id' => $id])
            ->asArray()
            ->one();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetall()
    {
        $model = NoticeModel::find()
            ->orderBy('id DESC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
}
