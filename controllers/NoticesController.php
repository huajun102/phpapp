<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\NoticesModel;

class NoticesController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionAddnotice()
    {
        $infoid = Yii::$app->request->post('infoid');
        $openid = Yii::$app->request->post('openid');
        $userinfo = Yii::$app->request->post('userinfo');
        
 
        // Yii::$app->db->createCommand()->insert('message',[
        //     'image'=>$image,
        //     'title'=>$title,
        //     'content'=>$content
        //     ])->execute();

        // 新建一条记录
        $model = new NoticesModel;
        $model->openid = $openid;
        $model->userinfo = $userinfo;
        $model->infoid = $infoid;
        
        $model->save();

        $data = array(
            'code' => 0,
            'message' => '新增成功',
            'result' => $model
        );
        $json = json_encode($data);
        return $json;
    }

    public function actionGetnoticesbyinfoid()
    {
        $infoid = Yii::$app->request->post('infoid');
        $model = NoticesModel::find()
            ->where(['infoid' => $infoid])
            ->orderBy('id DESC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
}
