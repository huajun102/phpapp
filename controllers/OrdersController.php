<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\OrdersModel;

class OrdersController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionAddorder()
    {
        $repeatable = Yii::$app->request->post('repeatable');
        $openid = Yii::$app->request->post('openid');
        $nickname = Yii::$app->request->post('nickname');
        $projectid = Yii::$app->request->post('projectid');
        $title = Yii::$app->request->post('title');
        $ordersdate = Yii::$app->request->post('ordersdate');
        $time1 = Yii::$app->request->post('time1');
        $time2 = Yii::$app->request->post('time2');
        $userInfo = Yii::$app->request->post('userInfo');
 
        $today = date("Y-m-d");
        $date1 = $ordersdate.' '.$time1;
        $date2 = $ordersdate.' '.$time2;
        
        
        if($repeatable == 1){
            Yii::$app->db->createCommand()->insert('orders',[
                'openid'=>$openid,
                'nickname'=>$nickname,
                'projectid'=>$projectid,
                'title'=>$title,
                'ordersdate'=>$ordersdate,
                'time1'=>$date1,
                'time2'=>$date2,
                'userInfo'=>$userInfo
                ])->execute();

                $data = array(
                    'code' => 0,
                    'message' => '预约成功',
                    'result' => new \stdClass 
                );
        }else{
            $items = OrdersModel::find()
            ->where([
                'openid' => $openid,
                'projectid' => $projectid
            ])
            ->asArray()
            ->all();

            if(count($items)>0){
                $data = array(
                    'code' => -1,
                    'message' => '已经预约',
                    'result' => new \stdClass 
                );
            }

            if(count($items) == 0){
                Yii::$app->db->createCommand()->insert('orders',[
                'openid'=>$openid,
                'nickname'=>$nickname,
                'projectid'=>$projectid,
                'title'=>$title,
                'ordersdate'=>$ordersdate,
                'time1'=>$date1,
                'time2'=>$date2,
                'userInfo'=>$userInfo
                ])->execute();

                $data = array(
                    'code' => 0,
                    'message' => '预约成功',
                    'result' => new \stdClass 
                );
            }
        }
        



        $json = json_encode($data);
        return $json;
    }
    public function actionGetordersbytoday()
    {
        $projectid = Yii::$app->request->post('projectid');
        if(empty($projectid)){
        $model = OrdersModel::find()
            ->orderBy('id DESC')
            ->asArray()
            ->all();
        }else{
            $today = date("Y-m-d");
            $model = OrdersModel::find()
            ->where([
                'projectid' => $projectid,
                'ordersdate' => $today
            ])
            ->orderBy('id DESC')
            ->asArray()
            ->all();
        }


        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetorders()
    {
        $projectid = Yii::$app->request->post('projectid');
        if(empty($projectid)){
        $model = OrdersModel::find()
            ->orderBy('id DESC')
            ->asArray()
            ->all();
        }else{
            $model = OrdersModel::find()
            ->where([
                'projectid' => $projectid
            ])
            ->orderBy('id DESC')
            ->asArray()
            ->all();
        }


        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetordersbynickname()
    {
        $nickname = Yii::$app->request->post('nickname');
        $model = OrdersModel::find()
            ->where(['nickname' => $nickname])
            ->orderBy('id DESC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetordersbyopenid()
    {
        $openid = Yii::$app->request->post('openid');
        $model = OrdersModel::find()
            ->where(['openid' => $openid])
            ->orderBy('id DESC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
}
