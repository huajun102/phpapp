<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\PhoneinfoModel;

class PhoneinfoController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionAddphoneinfo()
    {
        $phoneinfo = Yii::$app->request->post('phoneinfo');

        // 新建一条记录
        $model = new PhoneinfoModel;
        $model->phoneinfo = $phoneinfo;
        
        $model->save();
    

        $data = array(
            'code' => 0,
            'message' => '新增成功',
            'result' => new \stdClass 
        );
        $json = json_encode($data);
        return $json;
    }
    public function actionGetphoneinfo()
    {
        $model = PhoneinfoModel::find()
            ->orderBy('id DESC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetphoneinfobyid()
    {
        $id = Yii::$app->request->post('id');
        $model = PhoneinfoModel::find()
            ->where(['id' => $id])
            ->orderBy('id DESC')
            ->asArray()
            ->one();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetphoneinfobybrandname()
    {
        $brandname = Yii::$app->request->post('brandname');
        $model = PhoneinfoModel::find()
            ->where(['brandname' => $brandname])
            ->orderBy('sortcode DESC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
}
