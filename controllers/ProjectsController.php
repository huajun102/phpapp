<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\ProjectsModel;

class ProjectsController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAddproject()
    {
        $repeatable = Yii::$app->request->post('repeatable');
        $projecttype = Yii::$app->request->post('projecttype');
        $openid = Yii::$app->request->post('openid');
        $userinfo = Yii::$app->request->post('userinfo');
        $image = Yii::$app->request->post('image');
        $title = Yii::$app->request->post('title');
        $content = Yii::$app->request->post('content');

	    Yii::$app->db->createCommand()->insert('projects',[
            'repeatable'=>$repeatable,
            'type'=>$projecttype,
            'openid'=>$openid,
            'userinfo'=>$userinfo,
            'image'=>$image,
            'title'=>$title,
            'content'=>$content
            ])->execute();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => new \stdClass 
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetprojects()
    {
        $model = ProjectsModel::find()
            ->where(['public' => 1])
            ->orderBy('id DESC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetprojectsbyid()
    {
        $id = Yii::$app->request->post('id');
        $model = ProjectsModel::find()
            ->where(['id' => $id])
            ->asArray()
            ->one();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetprojectsbyopenid()
    {
        $openid = Yii::$app->request->post('openid');
        $model = ProjectsModel::find()
            ->where(['openid' => $openid])
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
}
