<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\QuestionModel;

class QuestionController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetquestions()
    {
        $model = QuestionModel::find()
            ->orderBy('id ASC')
            ->asArray()
            ->all();
        shuffle($model);

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => array_slice($model, 0, 10)
        );

        $json = json_encode($data);
        return $json;
    }

    public function actionGetquestionsbycode()
    {
        $code = Yii::$app->request->post('code');
        $model = QuestionModel::find()
            ->where(['examcode' => $code])
            ->orderBy('id ASC')
            ->asArray()
            ->all();
        
        shuffle($model);

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => array_slice($model, 0, 10)
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetquestionbyid()
    {
        $id = Yii::$app->request->post('id');
        $model = QuestionModel::find()
            ->where(['id' => $id])
            ->asArray()
            ->one();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGeneratedata()
    {
        $code = Yii::$app->request->post('code');
        $model = QuestionModel::find()
            ->select(['id'])
            ->where(['examcode' => $code])
            ->orderBy('id ASC')
            ->asArray()
            ->all();
        // $data = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38);
        $data=array();
        foreach ($model as &$item) {
            array_push($data, $item['id']);
        }
        shuffle($data);

        $obj= array(
            'code' => 0,
            'message' => '生成随机数字成功',
            'result' => array_slice($data, 0, 10)
        );

        $json = json_encode($obj);
        return $json;
    }
}
