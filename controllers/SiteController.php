<?php

namespace app\controllers;

use app\models\LogModel;
use app\models\UploadForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\models\Product;
use app\models\LogsModel;
use yii\web\UploadedFile;

class SiteController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionTest()
    {
        $model = new UploadForm();
        return $this->render('test', ['model' => $model]);
    }
    public function actionUploadfile()
    {
        $model = new UploadForm();
        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            // var_dump($model->imagefile);
            $obj = $model->upload();
            $json = json_encode($obj);
            return $json;
        }

    }
    public function actionUpload()
    {
        $model = new UploadForm();
        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            // var_dump($model->imagefile);
            if ($model->upload()) {
                // 文件上传成功
                return;
            }
        }

        return $this->render('upload', ['model' => $model]);
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * 提供接口专用测试.
     *
     * @return json
     */
    public function actionData()
    {
        $arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
        return json_encode($arr);

    }

    public function actionData2()
    {
        $arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
        return json_encode($arr);

    }

    /**
     * 提供接口专用测试.
     *
     * @return json
     */
    public function actionGetinfo()
    {
        $arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
        return json_encode($arr);

    }

    /**
     * 提供接口专用测试.
     *
     * @return json
     */
    public function actionGetname()
    {
        $arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
        return json_encode($arr);

    }

    /**
     * 提供接口专用测试.
     *
     * @return json
     */
    public function actionGetsome()
    {
        $arr = array(
            'Name'=>'希亚',
            'Age'=>20
        );
        $jsonencode = json_encode($arr);
        return $jsonencode;

    }

    public function actionGetconf()
    {
        // the name and value must be enclosed in double quotes
        // single quotes are not valid
        $data = Yii::$app->params;
        $json = json_encode($data);
        return $json;

    }

    public function actionSession()
    {
//        session_start();
        $_SESSION['name'] = 'jgl';
        var_dump(session_name());
        echo "<br>";
        var_dump(session_id());
        echo "<br>";
        var_dump(session_get_cookie_params());
        echo "<pre>";

        var_dump(session_get_cookie_params());
        exit();
    }

    public function actionGetinfo2()
    {
        session_start();
//        $_SESSION['name'] = 'jgl';
        var_dump(session_name());
        echo "<br>";
        var_dump(session_id());
//        echo "<br>";
//        var_dump(session_get_cookie_params());
//        echo "<pre>";

//        var_dump(session_get_cookie_params());
        exit();
    }
    public function actionQueryproduct()
    {

        $data = array(
            'code' => 0,
            'message' => '本接口已下线',
            'result' => ''
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetitems()
    {
        // the following will retrieve the user 'CeBe' from the database
        // $products = Product::find()->where(['id' => 1])->one();
        $keywords = Yii::$app->request->post('keywords');



        $model = Product::find()
        ->where("name like :keywords")
        ->addParams([':keywords'=>'%'.$keywords.'%'])
        ->limit(5)
        ->asArray()
        ->all();

        $type = '未分类';
        if(count($model)>0){
            $type = $model[0]['type'];
        }
        Yii::$app->db->createCommand()->insert('logs',['name'=>$keywords,'type'=>$type])->execute();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }    
    public function actionGetproductsbykeyword()
    {
        // the following will retrieve the user 'CeBe' from the database
        // $products = Product::find()->where(['id' => 1])->one();
        $keywords = Yii::$app->request->post('keywords');



        $model = Product::find()
        ->where("name like :keywords")
        ->addParams([':keywords'=>'%'.$keywords.'%'])
        ->limit(5)
        ->asArray()
        ->all();

        $type = '未分类';
        if(count($model)>0){
            $type = $model[0]['type'];
        }
        Yii::$app->db->createCommand()->insert('logs',['name'=>$keywords,'type'=>$type])->execute();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionQuerybyid()
    {
        // the following will retrieve the user 'CeBe' from the database
        // $products = Product::find()->where(['id' => 1])->one();
        $id = Yii::$app->request->get('id');

        $model = Product::find()
        ->where(['id' => $id])
        ->asArray()
        ->one();
        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }  
    public function actionGetbyid()
    {
        // the following will retrieve the user 'CeBe' from the database
        // $products = Product::find()->where(['id' => 1])->one();
        $id = Yii::$app->request->post('id');

        $model = Product::find()
        ->where(['id' => $id])
        ->asArray()
        ->one();
        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }  
    public function actionAddproduct()
    {
        // the following will retrieve the user 'CeBe' from the database
        // $products = Product::find()->where(['id' => 1])->one();
        $name = Yii::$app->request->get('name');
        $type = Yii::$app->request->get('type');

	    // Yii::$app->db->createCommand()->insert('products',['name'=>$name,'type'=>$type])->execute();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => new \stdClass 
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetproduct()
    {

        $model = LogsModel::find()
            ->where(['type' => ['干垃圾', '湿垃圾', '可回收物', '有害垃圾']])
            ->orderBy('id DESC')
            ->limit(5)
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetdata()
    {

        $data = Yii::$app->request->post();

        $obj= array(
            'code' => 0,
            'message' => '',
            'result' => $data
        );

        $json = json_encode($obj);
        return $json;
    }
    public function actionGeneratedata()
    {

        $data = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38);
        shuffle($data);

        $obj= array(
            'code' => 0,
            'message' => '生成随机数字成功',
            'result' => array_slice($data, 0, 10)
        );

        $json = json_encode($obj);
        return $json;
    }    
}
