<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\StudyModel;

class StudyController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionAdd()
    {
        $openid = Yii::$app->request->post('openid');
        $quid = Yii::$app->request->post('quid');
        $subjectcode = Yii::$app->request->post('subjectcode');
        $subjectname = Yii::$app->request->post('subjectname');


        $total = StudyModel::find()
        ->where(['openid' => $openid,'quid' => $quid])
        ->count();
        if($total==0){
            // 新建一条记录
            $model = new StudyModel;
            $model->openid = $openid;
            $model->quid = $quid;
            $model->subjectcode = $subjectcode;
            $model->subjectname = $subjectname;

            $model->save();

            $id =$model->id;
            $model = StudyModel::find()
                ->where(['id' => $id])
                ->orderBy('id DESC')
                ->asArray()
                ->one();

            $data = array(
                'code' => 0,
                'message' => '新增成功',
                'result' => $model
            );
        }else{
            $data = array(
                'code' => 0,
                'message' => '已经存在',
                'result' => date("Y-m-d H:i:s",time())
            );
        }
        

        $json = json_encode($data);
        return $json;
    }
    public function actionGetall()
    {
        $model = StudyModel::find()
            ->asArray()
            ->all();

        $json = json_encode($model);
        return $json;
    }
    public function actionGetbyopenid()
    {
        $openid = Yii::$app->request->post('openid');
        $model = StudyModel::find()
            ->where(['openid' => $openid])
            ->orderBy('id DESC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetitems()
    {
        $openid = Yii::$app->request->post('openid');
        $code = Yii::$app->request->post('code');
        $model = StudyModel::find()
            ->where(['openid' => $openid,'subjectcode' => $code])
            ->orderBy('id DESC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetjsonbyid()
    {
        $qid = Yii::$app->request->get('id');
        $model = StudyModel::find()
            ->select(['code', 'content', 'value'])
            ->where(['qid' => $qid])
            ->orderBy('id ASC')
            ->asArray()
            ->all();

        $json =  json_encode($model, JSON_UNESCAPED_UNICODE);
        echo($json);
    }    
}
