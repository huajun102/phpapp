<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\SubjectsModel;

class SubjectsController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionGetall()
    {
        $model = SubjectsModel::find()
            ->orderBy('id ASC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetbycode()
    {
        $code = Yii::$app->request->post('code');
        $model = SubjectsModel::find()
            ->where(['code' => $code])
            ->orderBy('id DESC')
            ->asArray()
            ->one();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }  
    public function actionGetbyexamcode()
    {
        $examcode = Yii::$app->request->post('examcode');
        $model = SubjectsModel::find()
            ->where(['examcode' => $examcode])
            ->orderBy('id ASC')
            ->asArray()
            ->all();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }      
}
