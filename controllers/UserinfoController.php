<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\UserinfoModel;

class UserinfoController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionAdduserinfo()
    {
        $openid = Yii::$app->request->post('openid');
        $userinfo = Yii::$app->request->post('userinfo');

        // 新建一条记录
        $model = new UserinfoModel;
        $model->openid = $openid;
        $model->userinfo = $userinfo;
        
        $model->save();
    

        $data = array(
            'code' => 0,
            'message' => '新增成功',
            'result' => new \stdClass 
        );
        $json = json_encode($data);
        return $json;
    }
    public function actionUpdateuserinfobyopenid()
    {
        $openid = Yii::$app->request->post('openid');
        $name = Yii::$app->request->post('name');
    

        // 新建一条记录
        $model = new UserinfoModel;
        $model->openid = $openid;
        $model->name = $name;
        
        $model::updateAll(['name' => $name], ['openid' => $openid]);
        
        // $model->update();

        $data = array(
            'code' => 0,
            'message' => '更新成功',
            'result' => $model
        );
        $json = json_encode($data);
        return $json;
    }
    public function actionGetuserinfobyopenid()
    {
        $openid = Yii::$app->request->post('openid');
        $model = UserinfoModel::find()
            ->where(['openid' => $openid])
            ->orderBy('id DESC')
            ->asArray()
            ->one();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
}
