<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\UsersModel;

class UsersController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionAdd()
    {
        $phone = Yii::$app->request->post('phone');
        $password = Yii::$app->request->post('password');
        $role = Yii::$app->request->post('role');



        // 新建一条记录
        $model = new UsersModel;
        $model->phone = $phone;
        $model->password = $password;
        $model->role = $role;
        
        $model->save();
        
        $id =$model->id;
        $model = UsersModel::find()
            ->where(['id' => $id])
            ->orderBy('id DESC')
            ->asArray()
            ->one();

        $data = array(
            'code' => 0,
            'message' => '新增成功',
            'result' => $model
        );
        $json = json_encode($data);
        return $json;
    }
    public function actionGet()
    {
        $phone = Yii::$app->request->post('phone');
        $password = Yii::$app->request->post('password');
        $model = UsersModel::find()
            ->where(['phone' => $phone,'password'=>$password])
            ->asArray()
            ->one();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $model
        );

        $json = json_encode($data);
        return $json;
    }
    
}
