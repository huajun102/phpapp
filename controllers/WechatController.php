<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\WechatModel;

class WechatController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetaccesstoken()
    {

        $WechatModel = new WechatModel("wxc06cce902e600a75", "5e2845ee0b637d56ed496bafa8f26842");
        $accessToken = $WechatModel->getAccessToken();
        $data = array(
            'code' => 0,
            'message' => '',
            'result' => array(
                'accessToken' => $accessToken
            )
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetinfo()
    {
        $code = Yii::$app->request->post('code');
        $WechatModel = new WechatModel("wxe06f54a073ac8c2b", "adf06928a597c551c1f0bf1b7b9eebee");
        $res = $WechatModel->getInfo($code);
        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $res
        );
        $json = json_encode($data);
        return $json;
    }
    public function actionGetaccesstoken2()
    {

        $WechatModel = new WechatModel("wxc06cce902e600a75", "7e1bf19a3b863ed75929e69e1707095d");
        $accessToken = $WechatModel->getAccessToken();
        $data = array(
            'code' => 0,
            'message' => '',
            'result' => array(
                'accessToken' => $accessToken
            )
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetinfo2()
    {
        $code = Yii::$app->request->post('code');
        $WechatModel = new WechatModel("wxc06cce902e600a75", "7e1bf19a3b863ed75929e69e1707095d");
        $res = $WechatModel->getInfo($code);
        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $res
        );
        $json = json_encode($data);
        return $json;
    }
    public function actionGetaccesstoken3()
    {

        $WechatModel = new WechatModel("wxdf3119dc0c196702", "44b243cca25b4c33449af50358bad6d2");
        $accessToken = $WechatModel->getAccessToken();
        $data = array(
            'code' => 0,
            'message' => '',
            'result' => array(
                'accessToken' => $accessToken
            )
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetinfo3()
    {
        $code = Yii::$app->request->post('code');
        $WechatModel = new WechatModel("wxdf3119dc0c196702", "44b243cca25b4c33449af50358bad6d2");
        $res = $WechatModel->getInfo($code);
        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $res
        );
        $json = json_encode($data);
        return $json;
    }
    public function actionGetaccesstoken4()
    {

        $WechatModel = new WechatModel("wx6b6ca27e05b94e3e", "92a9f62d0554d4286217394ef029cb51");
        $accessToken = $WechatModel->getAccessToken();
        $data = array(
            'code' => 0,
            'message' => '',
            'result' => array(
                'accessToken' => $accessToken
            )
        );

        $json = json_encode($data);
        return $json;
    }
    public function actionGetinfo4()
    {
        $code = Yii::$app->request->post('code');
        $WechatModel = new WechatModel("wx6b6ca27e05b94e3e", "92a9f62d0554d4286217394ef029cb51");
        $res = $WechatModel->getInfo($code);
        $data = array(
            'code' => 0,
            'message' => '',
            'result' => $res
        );
        $json = json_encode($data);
        return $json;
    }    
    public function actionCheckcontent(){
        $WechatModel = new WechatModel("wxe06f54a073ac8c2b", "adf06928a597c551c1f0bf1b7b9eebee");
        $access_token = $WechatModel->getAccessToken();

        $content = Yii::$app->request->post('content');

        $data = array(
            'content'=> $content
        );

        $result = $WechatModel->checkContent($data);
        $ret = array(
            'code' => 0,
            'message' => '',
            'result' => $result
        );

        $json = json_encode($ret);
        return $json;
    }
    public function actionGetzone(){
        $WechatModel = new WechatModel("wxc06cce902e600a75", "7e1bf19a3b863ed75929e69e1707095d");
        $location = Yii::$app->request->post('location');
        $result = $WechatModel->getZone($location);
        $ret = array(
            'code' => 0,
            'message' => '',
            'result' => $result
        );

        $json = json_encode($ret);
        return $json;
    }
    public function actionGetdata()
    {

        $data = Yii::$app->request->post();

        $obj= array(
            'code' => 0,
            'message' => '',
            'result' => $data
        );

        $json = json_encode($obj);
        return $json;
    }
}
