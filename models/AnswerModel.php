<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class AnswerModel extends \yii\db\ActiveRecord
{

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['qid','code', 'content', 'value'], 'required'],
            ['qid', 'string', 'length' => [1, 255]],
            ['code', 'string', 'length' => [1, 255]],
            ['content', 'string', 'length' => [0, 255]],
            ['value', 'string', 'length' => [1, 255]]
        ];
    } 
    
    public static function tableName()
    {
        return 'answer';
    }
   
}
