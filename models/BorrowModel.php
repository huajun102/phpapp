<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class BorrowModel extends \yii\db\ActiveRecord
{
    // public $image;
    // public $title;
    // public $content;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['openid','userinfo','phoneid','phoneinfo','action'], 'required'],
            ['openid', 'string', 'length' => [1, 255]],
            ['userinfo', 'string', 'length' => [1, 1000]],
            ['phoneid', 'string', 'length' => [1, 255]],
            ['phoneinfo', 'string', 'length' => [1, 10000]],
            ['action', 'string', 'length' => [1, 255]],
            
        ];
    } 
    
    public static function tableName()
    {
        return 'borrow';
    }
   
}
