<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class ConfModel extends \yii\db\ActiveRecord
{

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['openid','value'], 'required'],
            ['openid', 'string', 'length' => [1, 255]],
            ['value', 'string', 'length' => [1, 255]]
        ];
    } 
    
    public static function tableName()
    {
        return 'conf';
    }
   
}
