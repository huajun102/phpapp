<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class HistoryModel extends \yii\db\ActiveRecord
{

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['code','examname', 'questions', 'answers','records','score'], 'required'],
            ['openid', 'string', 'length' => [1, 255]],
            ['code', 'string', 'length' => [1, 255]],
            ['examname', 'string', 'length' => [1, 255]],
            ['questions', 'string', 'length' => [1, 255]],
            ['answers', 'string', 'length' => [1, 255]],
            ['records', 'string', 'length' => [1, 255]],
            ['score', 'string', 'length' => [1, 255]]
        ];
    } 
    
    public static function tableName()
    {
        return 'history';
    }
   
}
