<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class InfoModel extends \yii\db\ActiveRecord
{


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name','phone'], 'required'],
            ['name', 'string', 'length' => [1, 255]],
            ['phone', 'string', 'length' => [1, 255]]
            
        ];
    } 
    
    public static function tableName()
    {
        return 'info';
    }
   
}
