<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class MessageModel extends \yii\db\ActiveRecord
{
    // public $image;
    // public $title;
    // public $content;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['openid','userinfo','image', 'title', 'content'], 'required'],
            ['openid', 'string', 'length' => [1, 1000]],
            ['userinfo', 'string', 'length' => [1, 10000]],
            ['image', 'string', 'length' => [1, 255]],
            ['title', 'string', 'length' => [1, 255]],
            ['content', 'string', 'length' => [1, 10000]]
        ];
    } 
    
    public static function tableName()
    {
        return 'message';
    }
   
}
