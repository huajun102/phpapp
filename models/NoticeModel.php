<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class NoticeModel extends \yii\db\ActiveRecord
{
    // public $image;
    // public $title;
    // public $content;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['title','content'], 'required'],
            ['title', 'string', 'length' => [1, 255]],
            ['content', 'string', 'length' => [1, 1000]]
            
        ];
    } 
    
    public static function tableName()
    {
        return 'notice';
    }
   
}
