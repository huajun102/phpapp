<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class NoticesModel extends \yii\db\ActiveRecord
{
    // public $image;
    // public $title;
    // public $content;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['infoid','openid','userinfo'], 'required'],
            ['infoid', 'string', 'length' => [1, 255]],
            ['openid', 'string', 'length' => [1, 1000]],
            ['userinfo', 'string', 'length' => [1, 10000]]
            
        ];
    } 
    
    public static function tableName()
    {
        return 'notices';
    }
   
}
