<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class PhoneinfoModel extends \yii\db\ActiveRecord
{

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['phoneinfo'], 'required'],
            ['phoneinfo', 'string', 'length' => [1, 10000]]
        ];
    } 
    
    public static function tableName()
    {
        return 'phoneinfo';
    }
   
}
