<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class ProjectsModel extends \yii\db\ActiveRecord
{
    public $title;
    public $content;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [[ 'title', 'content'], 'required']
        ];
    } 
    
    public static function tableName()
    {
        return 'projects';
    }
   
}
