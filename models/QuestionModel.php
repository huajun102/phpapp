<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class QuestionModel extends \yii\db\ActiveRecord
{
    public $code;
    public $sortcode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

        ];
    } 
    
    public static function tableName()
    {
        return 'question';
    }
   
}
