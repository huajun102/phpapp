<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class RecordsModel extends \yii\db\ActiveRecord
{

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['openid','code','examname'], 'required'],
            ['openid', 'string', 'length' => [1, 255]],
            ['code', 'string', 'length' => [1, 255]],
            ['examname', 'string', 'length' => [1, 255]]
        ];
    } 
    
    public static function tableName()
    {
        return 'records';
    }
   
}
