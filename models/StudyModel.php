<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class StudyModel extends \yii\db\ActiveRecord
{

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['openid','quid','subjectcode','subjectname'], 'required'],
            ['openid', 'string', 'length' => [1, 255]],
            ['quid', 'string', 'length' => [1, 255]],
            ['subjectcode', 'string', 'length' => [1, 255]],
            ['subjectname', 'string', 'length' => [1, 255]]
        ];
    } 
    
    public static function tableName()
    {
        return 'study';
    }
   
}
