<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class SubjectsModel extends \yii\db\ActiveRecord
{

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'string', 'length' => [1, 255]]
        ];
    } 
    
    public static function tableName()
    {
        return 'subjects';
    }
   
}
