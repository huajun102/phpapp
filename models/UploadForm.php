<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
    * @var UploadedFile
    */
    public $imageFile;

    public function rules()
    {
        return [
        [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
//            var_dump( $this->imageFile);
//            exit();
            $this->imageFile->saveAs('/home/wwwroot/default/uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            $data = array(
                'code' => 0,
                'message' => '文件上传成功',
                'result' => $this->imageFile
            );
            return $data;
        } else {
            $data = array(
                'code' => -1,
                'message' => '文件上传失败',
                'result' => new \stdClass 
            );
            return $data;
        }
    }
}