<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class UserinfoModel extends \yii\db\ActiveRecord
{

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['openid','userinfo'], 'required'],
            ['openid', 'string', 'length' => [1, 1000]],
            ['name', 'string', 'length' => [1, 255]],
            ['userinfo', 'string', 'length' => [1, 10000]]
        ];
    } 
    
    public static function tableName()
    {
        return 'userinfo';
    }
   
}
