<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class UsersModel extends \yii\db\ActiveRecord
{
    // public $image;
    // public $title;
    // public $content;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['phone','password','role'], 'required'],
            ['phone', 'string', 'length' => [1, 255]],
            ['password', 'string', 'length' => [1, 255]],
            ['role', 'string', 'length' => [1, 255]]
            
        ];
    } 
    
    public static function tableName()
    {
        return 'users';
    }
   
}
