<?php
require_once "jssdk.php";
$jssdk = new JSSDK("wx4f81c66e3afd08c7", "0ec10ed2b46b5404e27417ec10d8c424");
$signPackage = $jssdk->GetSignPackage();
echo '<pre>';
print_r($signPackage);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title></title>
  <style type="text/css">
    button{
      width: 100%;
      height: 75px;
      margin: 25px 25px 25px 0;
      background-color: #CBF2F0;
    }
  </style>
</head>
<body>
  微信示例
  <button id="chooseImage">选择</button>
  <button id="uploadImage">上传</button>
  <!-- <button id="downloadImage">下载</button>   -->
  serverId:
  <input type="text" id="serverId" style="width: 100%;height: 50px;"/>
</body>
<script type="text/javascript" src="https://unpkg.com/jquery@3.4.1/dist/jquery.js"></script>
<script src="https://res.wx.qq.com/open/js/jweixin-1.4.0.js"></script>
<script>
  
  /*
   * 注意：
   * 1. 所有的JS接口只能在公众号绑定的域名下调用，公众号开发者需要先登录微信公众平台进入“公众号设置”的“功能设置”里填写“JS接口安全域名”。
   * 2. 如果发现在 Android 不能分享自定义内容，请到官网下载最新的包覆盖安装，Android 自定义分享接口需升级至 6.0.2.58 版本及以上。
   * 3. 常见问题及完整 JS-SDK 文档地址：http://mp.weixin.qq.com/wiki/7/aaa137b55fb2e0456bf8dd9148dd613f.html
   *
   * 开发中遇到问题详见文档“附录5-常见错误及解决办法”解决，如仍未能解决可通过以下渠道反馈：
   * 邮箱地址：weixin-open@qq.com
   * 邮件主题：【微信JS-SDK反馈】具体问题
   * 邮件内容说明：用简明的语言描述问题所在，并交代清楚遇到该问题的场景，可附上截屏图片，微信团队会尽快处理你的反馈。
   */
  wx.config({
    debug: true,
    appId: '<?php echo $signPackage["appId"];?>',
    timestamp: <?php echo $signPackage["timestamp"];?>,
    nonceStr: '<?php echo $signPackage["nonceStr"];?>',
    signature: '<?php echo $signPackage["signature"];?>',
    jsApiList: [
        // 所有要调用的 API 都要加到这个列表中
        'updateAppMessageShareData',
	      'updateTimelineShareData',
        'chooseImage',
        'uploadImage',
        'downloadImage'
    ]
  });
  wx.ready(function () {
    // 在这里调用 API
      wx.updateAppMessageShareData({
          title: '分享标题', // 分享标题
          desc: '分享描述', // 分享描述
          link: 'http://www.xiaomutong.com.cn/sample.php', // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
          imgUrl: 'http://www.xiaomutong.com.cn/assets/icon64_appwx_logo.png', // 分享图标
          success: function () {
              // 设置成功
		        setTimeout(function(){ 
              alert("Hello"); 
            }, 3000);
          }
      })
    wx.updateTimelineShareData({ 
        title: '分享标题', // 分享标题
        link: 'http://www.xiaomutong.com.cn/sample.php', // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
        imgUrl: 'http://www.xiaomutong.com.cn/assets/icon64_appwx_logo.png', // 分享图标
        success: function () {
          // 设置成功
          setTimeout(function(){ 
            alert("Hello"); 
          }, 3000);
        }
    })

  });

  $(document).ready(function(){
    console.log('hello');
    var localId;
    var localIds;
    var serverId;
    $('#chooseImage').click(function(){
      wx.chooseImage({
        count: 1, // 默认9
        // sizeType: ['original'],
        // 可以指定是原图还是压缩图，默认二者都有
        sizeType: ['original', 'compressed'], 
        sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
        success: function (res) {
          console.log(res);
          localIds = res.localIds; 
          // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
          wx.getLocalImgData({
            localId: localIds[0], // 图片的localID
            success: function (res) {
              console.log(res);
              var localData = res.localData; // localData是图片的base64数据，可以用img标签显示
            }
          });
        }
      });
    });
    $('#uploadImage').click(function(){
      localId = localIds[0];
      console.log(localId);
      wx.uploadImage({
        localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
        isShowProgressTips: 1, // 默认为1，显示进度提示
        success: function (res) {
          console.log(res);
          serverId = res.serverId; // 返回图片的服务器端ID
          alert(serverId);
          $('#serverId').val(serverId);
        }
      });
    });

    $('#downloadImage').click(function(){
      console.log(serverId);
      wx.downloadImage({
        serverId: serverId, // 需要下载的图片的服务器端ID，由uploadImage接口获得
        isShowProgressTips: 1, // 默认为1，显示进度提示
        success: function (res) {
          console.log(res);
          var localId = res.localId; // 返回图片下载后的本地ID
        }
      });

    });    

  });

</script>
</html>
