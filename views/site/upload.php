<?php

use app\models\UploadForm;
use yii\widgets\ActiveForm;
$model = new UploadForm();
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'imageFile')->fileInput() ?>

    <button>Submit</button>

<?php ActiveForm::end() ?>